// import Cookies from 'js-cookie'

// const TokenKey = 'Admin-Token'
const TokenKey = 'accessToken'

export function getToken() {
  // return Cookies.get(TokenKey)
  return sessionStorage.getItem('accessToken')
}

export function setToken(token) {
  return sessionStorage.setItem(TokenKey, token)
}

export function removeToken() {
  console.log('退出登录')
  sessionStorage.clear()
  return sessionStorage.removeItem(TokenKey)
}
